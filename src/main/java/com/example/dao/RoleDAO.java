package com.example.dao;

import com.example.model.Role;

public interface RoleDAO {
	
	public Role getRole(int id);

}
