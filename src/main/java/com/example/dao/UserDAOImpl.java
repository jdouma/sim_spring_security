package com.example.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	private Session openSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public User getUser(String login) {
		List<User> userList = new ArrayList<User>();
		Session session = openSession();
		Query query = session.createQuery("from User u where u.login = :login");
		query.setParameter("login", login);
		userList = query.list();
		if(userList.size() > 0 ) {
			return userList.get(0);
		} else {
			return null;
		}
	}

}
