package com.example.dao;

import com.example.model.User;

public interface UserDAO {
	public User getUser(String login);

}
