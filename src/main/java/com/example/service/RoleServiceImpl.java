package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.dao.RoleDAO;
import com.example.model.Role;
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleDAO roleDAO;
	public Role getRole(int id) {
		return roleDAO.getRole(id);	
	}

}
