package com.example.service;

import com.example.model.Role;

public interface RoleService {
	public Role getRole(int id);
}
